This project will allow for anyone to have the ability to use tensorflow's keras API to build a convolutional neural network to classify hand written digits.

**CREDITS** Thank you to Sentdex and Margaret Maynard-Reid for the inspiration and helpful documentation/videos