```python
    import tensorflow as tf
    print ("TensorFlow version: " + tf.__version__) #verify tensorflow version 1.4.0
    
    mnist = tf.keras.datasets.mnist #pull in mnist & data
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    
    x_train = tf.keras.utils.normalize(x_train, axis=1) #normalize data from 0-255 --> now 0-1
    x_test = tf.keras.utils.normalize(x_test, axis=1)
    
    model = tf.keras.models.Sequential() #build out your convolutional nueral network
    model.add(tf.keras.layers.Flatten(input_shape=(28,28))) #make sure to add the input shape dimensions.
    model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))
    
    model.compile(optimizer='adam',
                 loss='sparse_categorical_crossentropy',
                 metrics=['accuracy'])
    model.fit(x_train, y_train, epochs=3)#run epochs to ensure a successful training has occured. Watch to make sure data is not overfitting.
    
    val_loss, val_acc = model.evaluate(x_test, y_test)
    print(val_loss, val_acc)#print here to check your accuracy and loss amounts.
    
    import matplotlib.pyplot as plt
    #plt.imshow(x_train[0])
    #plt.imshow(x_train[0], cmap = plt.cm.binary) #shows black and white.
    plt.show()
    print(x_train[1])
    
    model.save('epic_num_reader.model')
    
    new_model = tf.keras.models.load_model('epic_num_reader.model')
    
    predictions = new_model.predict([x_test])
    print(predictions)
    
    import numpy as np
    print(np.argmax(predictions[501]))
    
    plt.imshow(x_test[501])
    plt.show()
```